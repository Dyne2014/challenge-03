const express = require("express");
const app = express();
const routes = require("./routes/routes");
const path = require("path");
app.use(express.static("public"));

app.set("view engine", "ejs");
app.set("view engine", "html");

app.use(express.static(path.join(__dirname, "css")));
app.use("/", routes);

// app listen ini harus dipaling bawah
app.listen(4000, () => {
  console.log("Server berjalan di port 4000");
});

module.exports = app;
