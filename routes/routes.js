const { Router } = require("express");
const path = require("path");
const router = Router();

router.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "../public/loginPage.html"));
});

router.get("/dashboard", (req, res) => {
  res.sendFile(path.join(__dirname, "../views/dashboard2.html"));
});

router.get("/car-list", (req, res) => {
  res.sendFile(path.join(__dirname, "../views/listCar.html"));
});

router.get("/car-add", (req, res) => {
  res.sendFile(path.join(__dirname, "../views/addNewCar.html"));
});

module.exports = router;
